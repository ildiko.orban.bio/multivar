---
title: Principal Coordinate Analysis
#author: Miguel Alvarez
date: '2024-01-26'
date-modified: '`r Sys.Date()`'
#image: iris-stats.png
categories:
  - method
description:
  Statistical descriptions of the data set iris.
draft: true
---

# Description

Principal Coordinate Analysis (PCoA), also known as classical multidimensional scaling (MDS), is a statistical technique used for visualizing and analyzing similarities or dissimilarities among a set of objects or observations.
PCoA transforms a distance matrix derived from pairwise dissimilarity measures into a set of orthogonal axes (principal coordinates) in a lower-dimensional space.
These axes represent the most important directions in the data that account for the maximum variance.
Unlike classical multidimensional scaling, PCoA can handle both metric and non-metric distance measures, making it versatile for various types of data.
PCoA is often employed in fields such as biology, ecology, and genetics, providing a useful tool for exploring patterns and relationships within complex datasets.

# Example


```{r}
#| echo: false
lalashan <- readRDS("../../data/lalashan.rds")
```

```{r}
#| eval: false
lalashan <- readRDS("data/lalashan.rds")
```

```{r}
#| output: false
library(vegan)

nmds_ord <- metaMDS(comm = lalashan$cross, distance = "bray")
```

```{r}
plot(nmds_ord)
```


# Alternative functions

- `vegan::metaMDS()`
- `MASS::isoMDS()`

# Exercises

TODO

