# Multivariate Ordination

- [Introduction](#introduction)
- [Render the page](#render-the-page)

# Introduction

Public material will be stored in this repository, which also hosts the
site for this workshop.

# Render the page

To render this page you will need following software installed in your
system:

- [R](ran.r-project.org/)
- [RStudio](https://posit.co/download/rstudio-desktop/)
- [pandoc](https://pandoc.org/)
- [Quarto](https://quarto.org/)

You have two options to render this page. The first is using your
command prompt or terminal.

``` bash
quarto render . --output-dir=public/
```

Then you can inspect a preview by using:

``` bash
quarto preview
```

The second option is by installing the `quarto` package in R and then
running:

``` r
library(quarto)

# Render the site
quarto_render()

# Open a preview
quarto_preview()

# Close the preview
quarto_preview_stop()
```
